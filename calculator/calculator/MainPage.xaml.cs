﻿using Xamarin.Forms;

namespace calculator
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void buttonHandler(object sender, System.EventArgs e)
        {
            result.Text = (sender as Button).Text.Equals("C") ?
                "" :
                (sender as Button).Text;
        }
    }
}
